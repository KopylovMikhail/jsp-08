package ru.kopylov.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kopylov.feign.client.TmClient;
import ru.kopylov.feign.dto.ProjectDto;
import ru.kopylov.feign.dto.TaskDto;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class TmController {

    @Autowired(required = false)
    private TmClient tmClient;

    @RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDto> getProjects() {
        return tmClient.getProjects();
    }

    @RequestMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto getProject(@PathVariable("id") String id) {
        return tmClient.getProject(id);
    }

    @RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDto> getTasks() {
        return tmClient.getTasks();
    }

    @RequestMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDto getTask(@PathVariable("id") String id) {
        return tmClient.getTask(id);
    }

}
