package ru.kopylov.tm.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Task;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class TaskRestController {
    
    @Autowired
    ITaskService taskService;

    @RequestMapping(value = "/tasks", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Task> getTasks() {
        return taskService.findAll();
    }

    @GetMapping(value = "/task/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Task getTask(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @PutMapping(value = "/task",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Task updateTask(@RequestBody Task task) {
        taskService.save(task);
        return task;
    }

    @DeleteMapping(value = "/task/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void deleteTask(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

    @PostMapping(value = "/task",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Task addTask(@RequestBody Task task) {
        taskService.save(task);
        return task;
    }
    
}
