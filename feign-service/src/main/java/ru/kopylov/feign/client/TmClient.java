package ru.kopylov.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kopylov.feign.dto.ProjectDto;
import ru.kopylov.feign.dto.TaskDto;

import java.util.List;

@RequestMapping(value = "/rest")
@FeignClient(name = "tm-server")
//@FeignClient(value = "tm-server", url = "http://localhost:8080")
public interface TmClient {

    @RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDto> getProjects();

    @RequestMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDto getProject(@PathVariable("id") String id);

    @RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDto> getTasks();

    @RequestMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDto getTask(@PathVariable("id") String id);

}
