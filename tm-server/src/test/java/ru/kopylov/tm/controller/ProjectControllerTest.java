package ru.kopylov.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import ru.kopylov.tm.TmApplication;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.enumerated.State;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TmApplication.class)
public class ProjectControllerTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void projectList() throws Exception {
        mockMvc.perform(get("/project_list"))
                .andExpect(status().isOk())
                .andExpect(view().name("projects"))
                .andExpect(forwardedUrl("/WEB-INF/views/projects.jsp"))
                .andExpect(model().attributeExists("projects"))
                .andExpect(model().attribute("projects", hasSize(projectService.findAll().size())));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void viewProject() throws Exception {
        final Project project = new Project();
        project.setName("TEST_NAME");
        projectService.save(project);
        final String url = "/project_view/" + project.getId();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(view().name("project_view"))
                .andExpect(forwardedUrl("/WEB-INF/views/project_view.jsp"))
                .andExpect(model().attributeExists("project"))
                .andExpect(model().attribute("project", hasProperty("id", is(project.getId()))))
                .andExpect(model().attribute("project", hasProperty("name", is(project.getName()))));
        projectService.deleteById(project.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void editProject() throws Exception {
        final Project project = new Project();
        project.setName("TEST_NAME");
        projectService.save(project);
        final String url = "/project_edit/" + project.getId();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(view().name("project_edit"))
                .andExpect(forwardedUrl("/WEB-INF/views/project_edit.jsp"))
                .andExpect(model().attributeExists("project"))
                .andExpect(model().attribute("project", hasProperty("id", is(project.getId()))))
                .andExpect(model().attribute("project", hasProperty("name", is(project.getName()))));
        projectService.deleteById(project.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void saveProject() throws Exception {
        final Project project1 = new Project();
        project1.setName("TEST_0001");
        project1.setState(State.PROCESS);
        projectService.save(project1);
        final String url = "/project_edit/edit";
        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", project1.getId())
                .param("name", "TEST_0002")
                .param("description", project1.getDescription())
                .param("dateStart", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()))
                .param("dateFinish", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()))
                .param("state", project1.getState().name())
        )
                .andExpect(status().is3xxRedirection());
        ModelAndView modelAndView = mockMvc.perform(get(url)).andReturn().getModelAndView();
        final Project project2 = projectService.findById(project1.getId());
        assertEquals(project1.getId(), project2.getId());
        assertNotEquals(project1.getName(), project2.getName());
        assertEquals("TEST_0002", project2.getName());
        projectService.deleteById(project1.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void removeProject() throws Exception {
        final Project project = new Project();
        projectService.save(project);
        final String url = "/project_remove/" + project.getId();
        mockMvc.perform(get(url))
                .andExpect(status().is3xxRedirection());
        assertNull(projectService.findById(project.getId()));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void createProject() throws Exception {
        final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()).substring(0, 10);
        mockMvc.perform(get("/project_create"))
                .andExpect(status().isOk())
                .andExpect(view().name("project_create"))
                .andExpect(forwardedUrl("/WEB-INF/views/project_create.jsp"))
                .andExpect(model().attributeExists("dateStart"))
                .andExpect(model().attributeExists("dateFinish"))
                .andExpect(model().attribute("dateStart", containsString(date)))
                .andExpect(model().attribute("dateFinish", containsString(date)));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void addProject() throws Exception {
        final String name = "TEST_0005";
        final String description = "DESCRIPTION_0005";
        final Date dateStart = new Date(5555L);
        final Date dateFinish = new Date(7777L);
        final String url = "/add_project";
        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", name)
                .param("description", description)
                .param("dateStart", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dateStart))
                .param("dateFinish", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dateFinish))
                .param("state", State.PROCESS.name())
        )
                .andExpect(status().is3xxRedirection());
        Project project = null;
        List<Project> projects = projectService.findAll();
        for (Project prj : projects) {
            if (name.equals(prj.getName()) &&
                    description.equals(prj.getDescription()) &&
                    dateStart.equals(prj.getDateStart()) &&
                    dateFinish.equals(prj.getDateFinish()) &&
                    State.PROCESS.equals(prj.getState())) {
                project = prj;
                break;
            }
        }
        assertNotNull(project);
        projectService.deleteById(project.getId());
    }

}