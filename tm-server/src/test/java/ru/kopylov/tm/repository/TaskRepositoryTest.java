package ru.kopylov.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.TmApplication;
import ru.kopylov.tm.entity.Task;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TmApplication.class)
public class TaskRepositoryTest {
    
    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void save() {
        final Task task = new Task();
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
        taskRepository.deleteById(task.getId());
    }

    @Test
    public void findById() {
        final Task task = new Task();
        taskRepository.save(task);
        assertEquals(taskRepository.findById("nope"), Optional.empty());
        assertNotNull(taskRepository.findById(task.getId()));
        taskRepository.deleteById(task.getId());
    }

    @Test
    public void deleteById() {
        final Task task = new Task();
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
        taskRepository.deleteById(task.getId());
        assertEquals(taskRepository.findById(task.getId()), Optional.empty());
    }

    @Test
    public void findAll() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskRepository.save(task1);
        taskRepository.save(task2);
        assertTrue(taskRepository.findAll().size() > 1);
        assertFalse(taskRepository.findAll().isEmpty());
        taskRepository.deleteById(task1.getId());
        taskRepository.deleteById(task2.getId());
    }

}