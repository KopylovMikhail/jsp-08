https://gitlab.com/KopylovMikhail/jsp-08

<b>SOFTWARE REQUIREMENTS:</b>
 - JRE 1.8
 
<b>TECHNOLOGY STACK:</b>
  - IntelliJ IDEA,
  - JAVA 8,
  - Maven 4.0,
  - Spring Boot 2.3.1
  - Eureka
  - Spring Cloud
  - Proxy Gateway Zuul
  - Feign
  
<b>DEVELOPER:</b>

 Mikhail Kopylov
<br> e-mail: kopylov_m88@mail.ru

<b>BUILD COMMANDS:</b>
```
mvn clean
```
```
mvn install
```
<b>RUN EUREKA:</b>
```
java -jar <PROJECT_FOLDER>\eureka-server\target\eureka-server-1.0.8.jar
```
<b>RUN CLOUD SERVER CONFIG:</b>
```
java -jar <PROJECT_FOLDER>\cloud-config\target\cloud-config-1.0.8.jar
```
<b>RUN TASK MANAGER:</b>
```
java -jar <PROJECT_FOLDER>\tm-server\target\tm-server-1.0.8.jar
```
<b>RUN PROXY GATEWAY ZUUL:</b>
```
java -jar <PROJECT_FOLDER>\zuul-gateway\target\zuul-gateway-1.0.8.jar
```
<b>RUN FEIGN SERVICE:</b>
```
java -jar <PROJECT_FOLDER>\feign-service\target\feign-service-1.0.8.jar
```