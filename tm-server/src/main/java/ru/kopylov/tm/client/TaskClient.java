package ru.kopylov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.entity.Task;

import java.util.List;

@FeignClient("task")
public interface TaskClient extends UserClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> getTasks(@RequestHeader("cookie") String sessionId);

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_XML_VALUE)
    List<Task> getTasksXml(@RequestHeader("cookie") String sessionId);

    @GetMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Task getTask(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @GetMapping(value = "/task/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    Task getTaskXml(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @PutMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    Task updateTask(@RequestHeader("cookie") String sessionId, Task task);

    @PutMapping(value = "/task", produces = MediaType.APPLICATION_XML_VALUE)
    Task updateTaskXml(@RequestHeader("cookie") String sessionId, Task task);

    @DeleteMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteTask(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @DeleteMapping(value = "/task/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    void deleteTaskXml(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @PostMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    Task addTask(@RequestHeader("cookie") String sessionId, Task task);

    @PostMapping(value = "/task", produces = MediaType.APPLICATION_XML_VALUE)
    Task addTaskXml(@RequestHeader("cookie") String sessionId, Task task);
    
}
