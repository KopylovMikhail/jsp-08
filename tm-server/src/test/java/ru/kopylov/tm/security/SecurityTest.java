package ru.kopylov.tm.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kopylov.tm.TmApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TmApplication.class)
public class SecurityTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void projectListAuth() throws Exception {
        mockMvc.perform(get("/project_list"))
                .andExpect(status().isOk())
                .andExpect(view().name("projects"))
                .andExpect(forwardedUrl("/WEB-INF/views/projects.jsp"))
                .andExpect(model().attributeExists("projects"));
    }

    @Test
    public void projectListUnAuth() throws Exception {
        mockMvc.perform(get("/project_list"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void loginExistUser() throws Exception {
        final String username = "test";
        final String password = "test";
        mockMvc.perform(post("/loginAction")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", username)
                .param("password", password)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void loginNonExistUser() throws Exception {
        final String username = "nope";
        final String password = "nope";
        mockMvc.perform(post("/loginAction")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", username)
                .param("password", password)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?error"));
    }

    @Test
    public void loginRestExistUser() throws Exception {
        final String userJson = "{\"login\":\"test\", \"passwordHash\":\"test\"}";
        mockMvc.perform(post("/rest/login")
                .content(userJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void loginRestNonExistUser() throws Exception {
        final String userJson = "{\"login\":\"nope\", \"passwordHash\":\"nope\"}";
        mockMvc.perform(post("/rest/login")
                .content(userJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void projectListRestUnAuth() throws Exception {
        mockMvc.perform(get("/projects"))
                .andExpect(status().is4xxClientError());
    }

}
