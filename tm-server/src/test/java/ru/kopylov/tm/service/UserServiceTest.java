package ru.kopylov.tm.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.TmApplication;
import ru.kopylov.tm.entity.User;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TmApplication.class)
public class UserServiceTest {
    
    @Autowired
    private UserService userService;

    @Test
    public void save() {
        final User user = new User();
        user.setLogin("TestLogin");
        userService.save(user);
        assertNotNull(userService.findByLogin(user.getLogin()));
        userService.deleteById(user.getId());
    }

    @Test
    public void saveWithParam() {
        final String login = "TestLogin";
        final String password = "TestPassword";
        userService.save(login, password);
        assertNotNull(userService.findByLogin(login));
        assertNull(userService.save(login, password));
        assertNull(userService.save(null, null));
        assertNull(userService.save("", ""));
        userService.deleteById(userService.findByLogin(login).getId());
    }

    @Test
    public void findByLogin() {
        final User user = new User();
        user.setLogin("TestLogin");
        userService.save(user);
        assertNull(userService.findByLogin("nope"));
        assertNotNull(userService.findByLogin(user.getLogin()));
        userService.deleteById(user.getId());
    }

    @Test
    public void deleteById() {
        final User user = new User();
        user.setLogin("TestLogin");
        userService.save(user);
        assertNotNull(userService.findByLogin(user.getLogin()));
        userService.deleteById(user.getId());
        assertNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void findAll() {
        final User user1 = new User();
        final User user2 = new User();
        userService.save(user1);
        userService.save(user2);
        assertTrue(userService.findAll().size() > 1);
        assertFalse(userService.findAll().isEmpty());
        userService.deleteById(user1.getId());
        userService.deleteById(user2.getId());
    }

}