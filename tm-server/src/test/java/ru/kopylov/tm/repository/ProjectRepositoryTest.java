package ru.kopylov.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.TmApplication;
import ru.kopylov.tm.entity.Project;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TmApplication.class)
public class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void save() {
        final Project project = new Project();
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
        projectRepository.deleteById(project.getId());
    }

    @Test
    public void findById() {
        final Project project = new Project();
        projectRepository.save(project);
        assertEquals(projectRepository.findById("nope"), Optional.empty());
        assertNotNull(projectRepository.findById(project.getId()));
        projectRepository.deleteById(project.getId());
    }

    @Test
    public void deleteById() {
        final Project project = new Project();
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
        projectRepository.deleteById(project.getId());
        assertEquals(projectRepository.findById(project.getId()), Optional.empty());
    }

    @Test
    public void findAll() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectRepository.save(project1);
        projectRepository.save(project2);
        assertTrue(projectRepository.findAll().size() > 1);
        assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.deleteById(project1.getId());
        projectRepository.deleteById(project2.getId());
    }

}