package ru.kopylov.feign.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.jackson.JsonComponent;
import ru.kopylov.feign.enumerated.State;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@JsonComponent
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDto {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Date dateStart;

    private Date dateFinish;

    private ProjectDto projectDto;

    private State state = State.PLANNED;

}
