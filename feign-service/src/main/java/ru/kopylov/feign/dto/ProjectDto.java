package ru.kopylov.feign.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.jackson.JsonComponent;
import ru.kopylov.feign.enumerated.State;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@JsonComponent
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDto {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date dateStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date dateFinish;

    private State state = State.PLANNED;

}
