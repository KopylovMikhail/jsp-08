package ru.kopylov.feign.enumerated;

import org.jetbrains.annotations.NotNull;

public enum State {

    PLANNED("PLANNED"),
    PROCESS("IN PROCESS"),
    DONE("DONE");

    @NotNull
    private final String displayName;

    State(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
